import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Imie2 {
	/*
	 * Utworzyć program który:
	 * - wczyta imie oraz nazwisko uzytkownika
	 * - obliczy sumę liter w imieniu i nazwisku
	 * - jeżeli suma będzie mniejsza niż 30 - wyświetli komunikat komunikat z ilością znaków w imieniu i nazisku
	 * - w przeciwnym wypadku wyświetli łączą ilość znaków (oóglnie) z adnotacją, iż imię i nazwisko jest bardzo długie
	 * - poprosi użytkownika o datę urodzin
	 * - poda odpowiedni znak zodiaku 
	 * ZADANIE DODATKOWE:
	 * - odwroci podaną datę użytkownika (z podanej np. 10.12.1999 na 1999.12.10)
	 */
	public static void main(String[] args) {
		//tworzymy potrzebne zminne; w tym wypadku 4 - dla imienia, nazwiska, daty urodzin oraz formatu daty 
		//format daty będziemy ustawiali AUTOMATYCZNIE na podstawie wpisanej przez uzytkownika daty
		String name,surname,userDate,dateFormat;
		//inicjalizujemy zmienne tekstowe jako puste (inijalizacja wmagana przez surname; reszta to dobry zwyczaj)
		name=surname=userDate=dateFormat="";
		//zmienna s pozwalająca na prosty sposób pobiedania danych z konsoli
		Scanner s = new Scanner(System.in);
		//przydatna tymczasowa zmienna tablica trzyelementowa ktora bedzie przechowywac date urodzin uzytkownika
		//generalnie mozna ja wyeliminowac
		String[] dmy=new String[3];
		System.out.print("Proszę podać imię i nazwisko: ");
		//oczekujemy, ze uzytkonik poda imie i nazwisko JAKO CALOSC!
		name = s.nextLine();
		//ponizsza petla ma za zadanie wyciac wiecej niz jedna spację zarówno pomiędzy imieniem(imionami) i nazwiskiem(nazwiskami)
		//jak i przed/za właściwym imieniem i nazwiskiem
		for(int i=0;i<name.length();i++) {
			//jeżeli spacja zostanie wykryta
			if (name.charAt(i)==' ') {
				//pierwsza jest dodawana jako właściwie podana
				surname+=name.charAt(i);
				//kolejne są eliminowane
				do {
					//chyba, że poniższy warunek (wskazujący, że jesteśmy właśnie na ostatnim elemencie)
					//będzie prawdziwy; wtedy przerywamy działanie pętli
					if (i==name.length()-1)
						break;
				} while(name.charAt(++i)==' ');
			}
			//bezwzględnie każde wykonanie pętli dodaje nam znak do zmiennej mającej przechowywać nazwisko
			surname+=name.charAt(i);
		}
		//poniższą linię można włączyć; pokaże ona oryginalny ciąg znakowy (ze wszystkimi spacjami) oraz po obróbce
		//System.out.print("\nImię i nazwisko -" + name + "; po obcięciu spacji" + surname);
		//na koniec przenosimy obrobiony tekst z powrotem do zmiennej name
		name=surname;
		//kolejna zmienna pomocnicza; dodana wedle zasady "zajmij więcej pamięci operacyjnej jednak wskazaną operację
		//(rozdzielenie ciągu znakowego po spacji) zrób raz
		String[] names = name.split(" ");
		//jako nazwisko traktujemy OSTATNI element utworzonyej wyżej tablicy; w tym celu jako numer tablicy
		//podajemy liczbę wszystkich elementów w niej zgromadzonej (names.length) - 1 (bo numerujemy od 0, nie 1)
		surname = name.split(" ")[names.length-1];
		//tutaj następuje wyświetlenie wiadomości o długości imienia i nazwiska jeżeli suma (imię + nazwisko)
		//wynosi do 30 znaków; założone zostało iż ciąg imienia i nazwiska zawiera tylko jedną spację
		if((name.length()-1) < 30) 
			System.out.print("\nTwoje imię posiada " + 
					(name.length() - surname.length() - 1) + 
					" znakow, natomist nazwisko " +
					surname.length() + " znaków.");		
		else
			System.out.print("\nTwoje imię i nazwisko posiada łącznie " +
					(name.length()-1) + " znaków. Twoje imię i nazwisko"
							+ " jest bardzo długie!");
//		2 część zadania - prosimy użytkownika o podanie daty urodzenia. Program przyjmuje daty jako dzien-miesiac-rok lub 
		//rok-miesiac-dzien; dodatkowo jako speraratów pól można używać: ./\ lub -
		System.out.print("\n\nPodaj swoją datę urodzenia: ");
		userDate = s.nextLine();
		//jeżeli data będzie zawierała mniej niż 8 znaków (8 to minimum);
		while(!(userDate.length()>=8)) {
			System.out.print("\nNieprawidłowa data. Podaj jeszcze raz: ");
			userDate = s.nextLine();
		}
		//zamknięcie strumienia wejściowego
		s.close();
		//ustawienie domyślnego separatora daty na -
		char separator='-';
		//jeżeli ciąg daty zawiera jeden z wyszczególnionych znaków, następuje zamiana separatora na podany
		if(userDate.contains("/")) {
			separator='/';
		}
		else if(userDate.contains("\\"))
			separator = '\\';
		else if (userDate.contains("/"))
			separator='/';
		else if (userDate.contains("."))
			separator='.';
		//następuje przepisanie do tablicy otrzymanej daty; trzeba pamiętać o konwersji separatora z pojedynczego znaku 
		//na ciąg znakowy (funkcja toString)
		dmy = userDate.split(Character.toString(separator));
		if (dmy.length==0)
			dmy = userDate.split("\\" + Character.toString(separator));
		//jeżeli pierwszy element tablicy jest 4 znakowy - pierwszy został wpisany rok;
		//ustalamy odpowiedni format daty do wprowadzenia
		if (dmy[0].length() ==4)
			dateFormat = "yyyy" + separator + "MM" + separator + "dd";
		else
			dateFormat = "dd" + separator + "MM" + separator + "yyyy";
		//tworzymy pomocniczy obiekt Calendar zarządzający datą
		Calendar cc = Calendar.getInstance();
		//próbujemy wykonać przypisanie daty do kalendarza poprzez funkcję setTime; ponieważ może ona wyrzucić błąd 
		//musi być otoczona klauzulą try-catch(-finally) 
		try {
			//tekst sprawdzający poprawność przypisania daty do SimpeDateFormat
			//System.out.print("\nDATA SIMPLE" + dateFormat + ": " + (new SimpleDateFormat(dateFormat).parse(userDate)));
			cc.setTime(new SimpleDateFormat(dateFormat).parse(userDate));
		}
		//obsługa wyrzuconego wyjątku; w tym wypadku, ponieważ nie zamierzamy nic z nim robić (a przypisać alternatywnie datę
		//poprzez funkcję set) używamy jako parametru standardowego wyjątku Exception (wyjątki będą omówione pózniej)
		catch(Exception e) {
			cc.set(Integer.parseInt(dmy[ dmy[0].length()==4 ? 0 : 2 ]),
			Integer.parseInt(dmy[1]),
			Integer.parseInt(dmy[ dmy[0].length()==4 ? 2 : 0 ]));
		}
		//tworzymy tablicę słów ze wszystkimi znakami zodiaku;
		String[] zodiacs = {"Baran (Aries)","Byk (Taurus)", //21.03 - 19.04/20.04-20.05
				"Bliznięta (Gemini)", "Rak (Cancer)", //21.05-20.06 / 21.06.22.07
				"Lew (Leo)", "Panna (Virgo)", "Waga (Libra)",//23.07-22.08/23.08-22.09/23.09-22.10
				"Skorpion (Scorpio)", "Strzelec (Sagittarius)",//23.10-21.11/22.11-21.12
				"Koziorożec (Capricorn)", "Wodnik (Aquarius)", "Ryby (Pisces)"};//22.12-19.01/20.01-18.02/19.02-20.03
		//druga tablica - tym razem dwuwymiarowa; pierwszy wymiar jest równy ilości znaków zodiaku
		//drugi to daty graniczne poszczególnych znaków (dzień rozpoczęcia oraz dzień zakończenia danego znaku
		//oznacza to, że tablica ma ogólnie wymiar 12x2
		String[][] zodiacDates = {
				{"21.03","19.04"}, //baran
				{"20.04","20.05"}, //byk
				{"21.05","20.06"}, //bliznieta
				{"21.06","22.07"}, //rak
				{"23.07","22.08"}, //lew
				{"23.08","22.09"}, //panna
				{"23.09","22.10"}, //waga
				{"23.10","21.11"}, //skorpion
				{"22.11","21.12"}, //strzelec
				{"22.12","19.01"}, //koziorozec
				{"20.01","18.02"}, //wodnik
				{"19.02","20.03"}}; //ryby
		//pętla przyrównująca datę podaną przez użytkownika (przetworzona do obiektu Calendar) do dat granicznych znaków zodiaku
		for (int i=0;i<zodiacDates.length;i++) {
			//opcja pierwsza - zgadza się miesąc z daty rozpoczęcia wskazanego znaku; jeżeli się zgadza....
				if (cc.get(Calendar.MONTH)==Integer.parseInt(zodiacDates[i][0].split("\\.")[1])) {
					//sprawdzany jest dzień; jeżeli jest on równy bądz większy od podanego przez użytkownika
					//to znaczy, że znalelismy odpowiedni znak zodiaku
					if (cc.get(Calendar.DAY_OF_MONTH)>=Integer.parseInt(zodiacDates[i][0].split("\\.")[0])) {
						System.out.print("Twój znak zodiaku to: " + zodiacs[i]);
						break;
					}
				}
				//sprawdzamy drugą datę graniczną; 
				if (cc.get(Calendar.MONTH)==Integer.parseInt(zodiacDates[i][1].split("\\.")[1])) {
					//teraz warunek ma odwroty kierunek (dzień musi być "młodszy" lub równy)
					if (cc.get(Calendar.DAY_OF_MONTH)<=Integer.parseInt(zodiacDates[i][1].split("\\.")[0])) {
						System.out.print("Twój znak zodiaku to: " + zodiacs[i]);
						break;
					}
				}
		}
		//petla wypisuje wszystkie znaki zodiaku oraz ich daty graniczne; wstęp do generowania losowych horoskopów
		for (int i=0;i<zodiacs.length;i++) {
			System.out.print("\nHoroskop dla " + zodiacs[i] + " (" +
					zodiacDates[i][0] + " - " + zodiacDates[i][1] + ")\n");
		
		}
		//ostatni fragment programu - odwórcenie daty wprowadzonej przez użytkownika
		System.out.print("\nOdwrócona data " + userDate + ": ");
		if (dmy[0].length()==4) {
			System.out.print(cc.get(Calendar.DAY_OF_MONTH)+
					Character.toString(separator)+
					(cc.get(Calendar.MONTH)+1)+
					Character.toString(separator)+
					cc.get(Calendar.YEAR));
		}
		else {
			System.out.print(cc.get(Calendar.YEAR)+
					Character.toString(separator)+
					(cc.get(Calendar.MONTH)+1)+
					Character.toString(separator)+
					cc.get(Calendar.DAY_OF_MONTH));
		}

		
	}	
	
}
