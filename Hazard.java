import java.util.Random;
import java.util.Scanner;


/*
 * Program ma za zadanie pokazać wykorzystanie tablicy jako sposobu zapisu większej ilości danych 
 * wskazanego typu; tablice pozwalają na wygodniejsze dodawanie wartości (np. poprzez pętlę for),
 * umożliwają pisanie mniejsze ilości kodu oraz mogą przyczynić się do dynamicznej zmiany ilości zapisanych wartości
 * w trakcie użytkowania programu (użytkownik może nam np. podać rozmiar tworzonej tablicy i na jego podstawie
 * utworzyć zakres do losowania danych)
 */
public class Hazard {
	public static void main(String[] args) {
		
		System.out.print("Podaj ilość liczb do wylosowania:");
		//poniżej tworzona jest zmienna, która odpowiada za rozmiar tablicy liczb (ilość liczb do wylosowania)
		//użytkownik podaje ją sam; poniższy sposób pokazuje, że możemy "w linii" utowrzyć objekt Scanner
		//odpowiedzialny za pobranie wartości Int; ponieważ otrzymywalibyśmy ostrzeżenie (że utworzone zródło 
		//Scanner nie ma nigdzie zamknięcia) użyte zostało nadpisanie ostrzeżenia dotyczące zasobu (brak ostrzeżeń
		//o potencjalnym problemie - stosować tylko w ekstrajasnych sytuacjach)
		@SuppressWarnings("resource")
		int arraySize =new Scanner(System.in).nextInt();
		//następuje utworzenie zmiennej tablicowej o nazwie numbers; tablica będzie odpowiedzialna za przechowywanie
		//tylu wartości, ile podane zostało przez użytkownika. jest to najprostszy sposób na dynamiczne lokowanie
		//zmiennych motytowane działaniem programu
		int[] numbers = new int[arraySize];
	
		
		//pętla, w której nastąpi przypisanie wartości do niedawno utworzonej tablicy
		for(int i=0;i<numbers.length;++i) {
			int tmp = 0;
			//ponieważ chcemy mieć unikatowe losowania - tworzmy mienną pomocniczą unique, która będzie sprawdzać czy wysolowana
			//niedawno liczba nie figruje w tabeli
			boolean unique = false;
			//poniższy komentarz z kodem jest odpowiednikiem tego użytego w kodzie; ponieważ pozytywne spełnienie 
			//warunku wejściowego instrukcji warunkowych i/lub pętli daje ZAWSZE PRAWDA (True), np.
			//while(true) {...} ZAWSZE WYKONA KOD w nawiasach kamrowych podczas gry
			//while(false) {...} NIGDY NIE WYKONA KODU w nawisach klamrowych
			//stąd do chwili, kiedy nie zostanie wylosowana unikatowa liczba i unique będzie oryginalnie posiadać wartość false
			//wykrzyknik przed tą zmienną zmieni jej wartość na przeciwną (false -> true) w badaniu warunku (tzw. negator wyniku)
			//wynikiem jest krótszy, tak samo funkcjonlany warunek
	//		while (unique == false) {
			while (!unique) {
				//losujemy liczbę z przedziału wielkosc tablicy *3 (np. przy wielkości tablicy 5 -> 5*3=15
				tmp = new Random().nextInt(arraySize*3);
				unique=true; //zmieniamy wartośc unique na true; zakładamy, że jest unikatowa
				//pętla przepatrująca wszystkie elementy tablicy
				for(int j=0;j<numbers.length;j++) {
					//jeżeli wylosowana wartość jest już w tablicy na którymkolwiek elemencie....
					if (tmp == numbers[j]) {
						//ustalamy, że obcenie wylosowana wartość nie jest jednak unikatowa
						unique=false;
						//poniższa komenda PRZERYWA NATYCHMIASTOWO pętlę, w ramach której wykonywany jest warunek
						//(po co patrzeć na kolejne elementy, skoro znaleziono szukaną wartość)
						break;
					}
				}
				
			}
			//jeżeli wartość jest rzeczywiście uniktowa to wpisujemy ją do tablicy pod kolejnym jej indeksem (numrem).
			numbers[i] = tmp;
		}
		
		//wyświetlamy wszystkie wylosowane liczby
		String result = "Wylosowane dziś liczby to: ";
		
		for (int i=0;i<numbers.length;i++) {
			//poniżej zakomentowany kod odpowiada temu obecnie użytemu
			//zamiast pisać iż do jakiejś zmiennej (w przykładzie poniżej dotyzy to ciągu znakowego) dodajemy (dopisujemy) zawartość
			//to zawsze musimy najpierw przepisać aktualną zawartość i dopisać jej nową część. Operator += zastępuje takie działanie
			//nie ma wplywu na szykość lecz na prostszą postać kodu
			//result = result + numbers[i];
			result += numbers[i] + ", ";
		}
		//na sam koniec usuwamuy ostatni dodany przeciek do ciągu znakowego (nie wyglądał zbyt estetycznie)
		result = result.substring(0, result.length()-2);
		System.out.print("\n" + result);	
	}
}
