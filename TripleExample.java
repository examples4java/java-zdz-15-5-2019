import java.util.Random;
import java.util.Scanner;

/*
 * Przykład pokazuje użycie operatora trójstanowego, którego działanie jest podobne do if() {} else {} z tą jednak różnicą,
 * że można go używać w niemal dowolnym fragmencie kodu - nawet wewnątrz innej operacji czy też operacji na wynikach.
 * Poniżej będzie pokazane działanie wklejania fragmentu tekstu bezpośrednio podczas wyświetlania tekstu. 
 */
public class TripleExample {
	public static void main(String[] args) {
		int a,b;
		a=b=0;
		//prosimy użytkownika o 2 liczby całkowite
		System.out.print("Podaj dwie liczby całkowite: ");
		Scanner s = new Scanner(System.in);
		
		a = s.nextInt();
		b = s.nextInt();
		s.close();
		//po wyświetleniu obu podanych liczb informujemy użytkownika czy pierwsza liczba była większa czy mniejsza od
		//drugiej; waraunek porównania został wprowadzony bezpośrednio w metodzie wyświetlającej tekst na konsoli użytkownika
		//jeżeli będzie prawdziwy (pierwsza liczba będzie większa od drugiej) wyświetli się napis
		//"Z czego liczba a jest większa od b!" -> zostanie wstawiony fragment tekstu znajdujący się PO ZNAKU ZAPYTANIA
		//w przeciwnym wypadku wstawiony zostanie framgent PO ZNAKU DWUKROPKA ("Z czego liczba a jest mniejsza od b!")
		System.out.print("\nPodałeś następujące liczby: " + a + " oraz " + b+
				". Z czego liczba a jest " +
				(a>b ? "większa" : "mniejsza") +
				" od b!");
		//tutaj druga część kodu, w której to program losuje zmienne a i b z zakresu 0..10 (w nextInt() została podana maksymalna wartość)
		//kod będzie się powtarzał do chwili gdy a będzie różne od b
		//INFORMACJA: W przeciwieństwie do while() {} do {} while(); wykonuje się ZAWSZE PRZYNAJMNIEJ JEDEN RAZ. Warunek sprawdzany
		//jest na KOŃCU pętli, nie zas NA POCZĄTKU (jak to ma miejsce w while()). 
		do {		
			a = new Random().nextInt(10);
			b = new Random().nextInt(10);			
			//poniżej pokazany jest przykład operatora trójstanowego zagnieżdżonego (dwa poziomy)
			//pierwszy poziom bada czy wylosowane a jest większe od b; jeżeli tak posjawi się słowo "większa";
			//jeżeli warunek będzie fałszywy to po dwukropku pojawia się kolejny operator z testem czy a jest równe b)
			//jeżeli będzie równe - pojawi się stosowne słowo w ciągu; jeżeli zadanie będzie fałszywe - pojawi się słowo
			//"mniejsza"
			System.out.print("\nWylosowane liczby to " + a + " oraz " + b +
					". Liczba a jest " +
					(a>b ? "większa" : (a==b ? "równa" : "mniejsza")) +
					" od b!");
		} while(a!=b);		//gdy a==b -> program się zakończy
	}
}
